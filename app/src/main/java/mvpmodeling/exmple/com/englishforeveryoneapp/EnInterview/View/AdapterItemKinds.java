package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import mvpmodeling.exmple.com.englishforeveryoneapp.R;

/**
 * Created by kevin on 4/8/2016.
 */
public class AdapterItemKinds extends RecyclerView.Adapter<AdapterItemKinds.ViewHolder> {
    ArrayList<String> Data;

    public AdapterItemKinds(ArrayList<String> data) {
        Data = data;
    }

    @Override
    public AdapterItemKinds.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_en_items_kinds_card,parent,false);
        ViewHolder viewHolder = new ViewHolder(v);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterItemKinds.ViewHolder holder, int position) {
        holder.setData(Data.get(position));
    }

    @Override
    public int getItemCount() {
        return Data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView)itemView.findViewById(R.id.info_text);
        }
        public void setData(String data)
        {
            textView.setText(data);
        }
    }
}
