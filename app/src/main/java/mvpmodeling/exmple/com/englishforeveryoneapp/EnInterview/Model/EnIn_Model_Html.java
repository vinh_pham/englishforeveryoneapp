package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model;

import android.content.res.AssetManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Presenter.IEnIn_Presenter;
import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View.EnInterview;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Phrase;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Section;

/**
 * Created by kevin on 4/3/2016.
 */
public class EnIn_Model_Html {
    IEnIn_Presenter ipressenter;
    private int counter = 0;

    public EnIn_Model_Html(IEnIn_Presenter ienIn_presenter) {
        ipressenter = ienIn_presenter;
    }

    public void InjectDataToSection(English_Phrase data_phrase, int i) throws IOException {
        English_Section section1 = data_phrase.getSection(0);
        AssetManager assetManager = EnInterview.context.getAssets();
        String[] listPath = assetManager.list("en_phrase/Section_1");


        InputStream inputStream = assetManager.open("Boredtodeath.html");

        try {
            Document doc = Jsoup.parse(inputStream, "UTF8","example.com");

            Elements table = doc.select("#GridView1");
            Elements tile = table.select("");

        } catch (IOException e) {
            e.printStackTrace();
        }

        /*
        String temp = "";
        for (String t: listLocales) {
            temp+=t + "\\";
           *//* InputStream fileHTML = assetManager.open("en_phrase/Section_1/" + t);



            Document doc = Jsoup.parse(new File(),"UTF-8");*//*
        }*/
    }


    public void InjectDataToAnswer(AtomicReference<ArrayList<SentenceInterview>> resultShortAnswerRef, AtomicReference<ArrayList<SentenceInterview>> resultlongAnswerRef, String AddressNameHTML) {

        ArrayList<SentenceInterview> resultShortAnswer = new ArrayList<SentenceInterview>();
        ArrayList<SentenceInterview> resultlongAnswer = new ArrayList<SentenceInterview>();

        SentenceInterview title;
        AssetManager assetManager = EnInterview.context.getAssets();
        try {
            InputStream inputStream = assetManager.open("interviews/" + AddressNameHTML);
            /// basicI/zinterview1.html
            Document doc = Jsoup.parse(inputStream, "UTF8", "");
            Elements table = doc.select("#GridView1");


            Elements explaination = table.select("td");
            Elements h1 = table.select("h1");

            Elements a = explaination.select("a");

            String tex = explaination.text();
            String print = tex.substring(tex.indexOf("Short Answers") + 14, tex.indexOf("Long Answer"));

            String[] listSlipt = print.split("\"");
            ArrayList<String> list2 = new ArrayList<>();

            for(int i = 0; i < listSlipt.length; i++)
            {
                if(!listSlipt[i].equals("") && !listSlipt[i].equals(" "))
                {
                    list2.add(listSlipt[i]);
                }
            }


            int size = list2.size();
            ipressenter.sendTemp(list2,a);
            boolean Tsize = true;

            for(Element el: a)
            {
                SentenceInterview sentence = new SentenceInterview(el.text(),el.attributes().get("href"));

                if(counter == 0)
                {
                    title = sentence;
                }else if(Tsize)
                {
                    resultShortAnswer.add(sentence);
                    size--;
                    if(size == 0)
                    {
                        Tsize = false;
                    }
                }else{
                    resultlongAnswer.add(sentence);
                }
                counter++;

            }

            /*resultlongAnswer.clear();
            resultlongAnswer.add(new SentenceInterview("TEst 1", "1231"));
            resultlongAnswer.add(new SentenceInterview("TEst 2","1231"));
            resultlongAnswer.add(new SentenceInterview("TEst 3","1231"));
            resultlongAnswer.add(new SentenceInterview("TEst 4","1231"));

            resultShortAnswer.clear();
            resultShortAnswer.add(new SentenceInterview("TEst 1", "1231"));
            resultShortAnswer.add(new SentenceInterview("TEst 2","1231"));
            resultShortAnswer.add(new SentenceInterview("TEst 3","1231"));
            resultShortAnswer.add(new SentenceInterview("TEst 4","1231"));
*/

            resultShortAnswerRef.set(resultShortAnswer);
            resultlongAnswerRef.set(resultlongAnswer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getKindItemsFromHTML() {
        ArrayList<String> result = new ArrayList<>();

        AssetManager assetManager = EnInterview.context.getAssets();
        try {
            InputStream inputStream = assetManager.open("interviews/basicI/listItemsKinds.html");
            Document document = Jsoup.parse(inputStream, "UTF8", "");

            Element div = document.select("div.list-page").first();
            Element divsInDiv = div.select("div[style]").get(1);


            Elements a = divsInDiv.select("a");

            for (Element t: a) {
                String text = t.text();
                result.add(text);
            }






        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
