package mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model;

import java.io.File;

/**
 * Created by kevin on 4/2/2016.
 */
public class English_Section {
    public English_Section(String sec_Name, double sec_Size) {
        this.sec_Name = sec_Name;
        this.sec_Size = sec_Size;
    }

    public String getSec_Name() {
        return sec_Name;
    }

    public void setSec_Name(String sec_Name) {
        this.sec_Name = sec_Name;
    }

    public double getSec_Size() {
        return sec_Size;
    }

    public void setSec_Size(double sec_Size) {
        this.sec_Size = sec_Size;
    }

    private String sec_Name;
    private double sec_Size;
    File faddress;


    public English_Section(String sec_Name, double sec_Size,  File Faddress) {
        this.sec_Name = sec_Name;
        this.sec_Size = sec_Size;
        this.faddress = Faddress;
    }
}
