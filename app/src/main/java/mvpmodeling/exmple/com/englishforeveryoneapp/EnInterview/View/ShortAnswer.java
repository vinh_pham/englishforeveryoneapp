package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model.SentenceInterview;
import mvpmodeling.exmple.com.englishforeveryoneapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ShortAnswer.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ShortAnswer#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShortAnswer extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private static final String ARDATA = "DATA";

    // TODO: Rename and change types of parameters
    ArrayList<SentenceInterview> AListShortAnswer;

    private OnFragmentInteractionListener mListener;
    private ListView my_list_view;

    public ShortAnswer() {
        // Required empty public constructor
    }



    // TODO: Rename and change types and number of parameters

    public static ShortAnswer newInstance(ArrayList<SentenceInterview> resultShortAnswer) {
        ShortAnswer fragment = new ShortAnswer();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARDATA, resultShortAnswer);
        fragment.setArguments(bundle);
        return fragment;
    }
    public static ShortAnswer newInstance() {
        ShortAnswer fragment = new ShortAnswer();
        return fragment;
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            AListShortAnswer = (ArrayList<SentenceInterview>) getArguments().getSerializable(ARDATA);

        }
        else {
            AListShortAnswer.add(new SentenceInterview("TEst 1", "1231"));
            AListShortAnswer.add(new SentenceInterview("TEst 2", "1231"));
            AListShortAnswer.add(new SentenceInterview("TEst 3","1231"));
            AListShortAnswer.add(new SentenceInterview("TEst 4","1231"));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_short_answer, container, false);
        my_list_view = (ListView) view.findViewById(R.id.my_list_view_short);




        if(AListShortAnswer != null) {

            AdapterListViewTesting adapter = new AdapterListViewTesting(getContext(),AListShortAnswer);
            my_list_view.setAdapter(adapter);

        }


        return view;
    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }




        /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



}
