package mvpmodeling.exmple.com.englishforeveryoneapp.Main.Presenter;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Phrase;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Systems;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Vocabulary;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.View.IMainActivity;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.View.MainActivity;

/**
 * Created by kevin on 4/2/2016.
 */
public class MainPresenter implements IMainPresenter {

    IMainActivity iMainActivity;

    public MainPresenter(MainActivity MainActivity) {
        this.iMainActivity = MainActivity;
    }

    public void GetListSystem() {
        ArrayList<English_Systems> result = new ArrayList<>();
        result.add(new English_Phrase("Phrase",123));
        result.add(new English_Vocabulary("Vocabulary",456));
        iMainActivity.receiveListSystem(result);
    }
}
