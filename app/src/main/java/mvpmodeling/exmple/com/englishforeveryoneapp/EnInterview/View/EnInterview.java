package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Presenter.EnIn_Presenter;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Phrase;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Section;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.View.RecyclerItemClickListener;
import mvpmodeling.exmple.com.englishforeveryoneapp.R;

public class EnInterview extends AppCompatActivity implements IEnInterview {

    private English_Phrase Data_Phrase;
    private EnIn_Presenter presenter;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter myAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public static Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_en_in);

        context = getApplicationContext();

        recyclerView = (RecyclerView)findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        Intent intent = getIntent();
        Data_Phrase = (English_Phrase) intent.getSerializableExtra("ENG_PHRASE");

        presenter = new EnIn_Presenter(this);

        try{

            presenter.InjectData(Data_Phrase);








        }catch (SAXException e) {
            Toast.makeText(getApplicationContext(),"ERROR 1",Toast.LENGTH_SHORT).show();
            Log.d("ERROR 1", e.toString());
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            Toast.makeText(getApplicationContext(),"ERROR 2",Toast.LENGTH_SHORT).show();

            Log.d("ERROR 2", e.toString());

            e.printStackTrace();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(),"ERROR 3: " + e.toString(),Toast.LENGTH_SHORT).show();

            Log.d("ERROR 3" , e.toString());

            e.printStackTrace();
        } catch (Exception e) {

            Toast.makeText(getApplicationContext(),"ERROR 4: " + e.toString(),Toast.LENGTH_SHORT).show();

            e.printStackTrace();
        }

    }

    @Override
    public void ReceivedNumberSection(ArrayList<English_Section> listSection, int numberSection) {
        //// // TODO: 4/2/2016

        myAdapter = new EnIn_Apdapter_Section(listSection);
        recyclerView.setAdapter(myAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this.getApplication(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click

                        switch (position) {
                            case 0:
                                Intent intent = new Intent(EnInterview.this, EnItemsKinds.class);


                                startActivity(intent);

                                break;
                            case 1:
                                break;
                        }

                    }
                })
        );
    }
}
