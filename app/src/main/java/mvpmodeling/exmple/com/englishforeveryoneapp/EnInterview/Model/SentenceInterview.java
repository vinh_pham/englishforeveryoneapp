package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model;

/**
 * Created by kevin on 4/4/2016.
 */
public class SentenceInterview  {
    public SentenceInterview(String sentence, String uri) {
        this.sentence = sentence;
        this.uri = uri;
    }

    String sentence;
    String uri;

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
