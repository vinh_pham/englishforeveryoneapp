package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Presenter;

import android.util.Log;

import org.jsoup.select.Elements;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

import javax.xml.parsers.ParserConfigurationException;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model.EnIn_Model;
import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model.EnIn_Model_Html;
import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model.SentenceInterview;
import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View.IEnInItemInterview;
import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View.IEnInterview;
import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View.IEnItemsKindsView;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Phrase;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Section;

/**
 * Created by kevin on 4/2/2016.
 */
public class EnIn_Presenter implements IEnIn_Presenter {

    EnIn_Model model;
    EnIn_Model_Html model_html;
    IEnInterview iEnInterview;
    IEnItemsKindsView iEnItemsKindsView;

    IEnInItemInterview iEnInItemInterview;

    public EnIn_Presenter(IEnInterview ienInterview) {
        iEnInterview = ienInterview;
        model = new EnIn_Model(this);
        model_html = new EnIn_Model_Html(this);
    }

    public EnIn_Presenter(IEnInItemInterview ienInItemInterview) {
        this.iEnInItemInterview = ienInItemInterview;
    }

    public EnIn_Presenter(IEnItemsKindsView iEnItemsKindsView) {
        this.iEnItemsKindsView = iEnItemsKindsView;
        model_html = new EnIn_Model_Html(this);

    }

    public void InjectData(English_Phrase data_phrase) throws IOException, SAXException, ParserConfigurationException, Exception {
        int NumberSection = 10; // get truoc
        ArrayList<English_Section> listSection = model.getListSection();

        data_phrase.setListSection(listSection);
      //  model_html.InjectDataToSection(data_phrase, NumberSection);

        Log.d("ENPH","InjectData");

        iEnInterview.ReceivedNumberSection(listSection, NumberSection);
    }


    // basicI/zinterview1.html
    public void GetListSentenceAnswer(String AddressNameHTML) {

        model_html = new EnIn_Model_Html(this);


        AtomicReference<ArrayList<SentenceInterview>> resultShortAnswerRef = new AtomicReference<ArrayList<SentenceInterview>>();
        AtomicReference<ArrayList<SentenceInterview>> resultLongAnswerRef = new AtomicReference<ArrayList<SentenceInterview>>();;



        model_html.InjectDataToAnswer(resultShortAnswerRef, resultLongAnswerRef, AddressNameHTML);





        iEnInItemInterview.sendList(resultShortAnswerRef.get(), resultLongAnswerRef.get());
    }

    @Override
    public void sendTemp(ArrayList<String> listSlipt, Elements a) {
        iEnInItemInterview.receiveTemp(listSlipt, a);
    }

    public void GetItemsListKinds() {

        ArrayList<String> data = model_html.getKindItemsFromHTML();

        iEnItemsKindsView.receiveListKinds(data);
    }
}
