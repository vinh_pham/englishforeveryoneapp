package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model.SentenceInterview;

/**
 * Created by kevin on 4/4/2016.
 */
public class AdapterTabStrip extends FragmentPagerAdapter {


    ArrayList<SentenceInterview> resultShortAnswer;
    ArrayList<SentenceInterview> resultLongAnswer;

    public AdapterTabStrip(FragmentManager fm, ArrayList<SentenceInterview> resultShortAnswer, ArrayList<SentenceInterview> resultlongAnswer) {
        super(fm);
        this.resultLongAnswer = resultlongAnswer;
        this.resultShortAnswer = resultShortAnswer;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position)
        {
            case 0:
                return ShortAnswer.newInstance(resultShortAnswer);
            case 1:
                return LongAnswer.newInstance(resultLongAnswer);
            default:
                return ShortAnswer.newInstance(resultShortAnswer);
        }

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position)
        {
            case 0:
                return "Short Answers";
            case 1:
                return "Long Answers";
        }
        return "";
    }
}
