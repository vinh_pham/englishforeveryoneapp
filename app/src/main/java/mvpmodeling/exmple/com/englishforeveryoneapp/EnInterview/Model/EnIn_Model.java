package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model;

import android.content.res.AssetManager;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Presenter.IEnIn_Presenter;
import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View.EnInterview;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Section;



/**
 * Created by kevin on 4/2/2016.
 */
public class EnIn_Model {

    private IEnIn_Presenter IEnIn_presenter;

    public EnIn_Model(IEnIn_Presenter ienIn_presenter) {
        setIEnIn_presenter(ienIn_presenter);
    }

    public ArrayList<English_Section> getListSection() throws ParserConfigurationException, IOException, SAXException, Exception {

        AssetManager assetManager = EnInterview.context.getAssets();
        InputStream file = assetManager.open("ListSection.xml");

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);
        doc.getDocumentElement().normalize();

        return CreateList( doc);
    }

    private ArrayList<English_Section> CreateList( Document doc) {
        ArrayList<English_Section> result = new ArrayList<English_Section>();
        NodeList root = doc.getElementsByTagName("section");

        for (int i = 0; i < root.getLength(); i++) {
            String name = "";
            double size = 0;

            Element sec = (Element) root.item(i);
            name = sec.getElementsByTagName("Name").item(0).getTextContent();
            size = Double.parseDouble(sec.getElementsByTagName("Size").item(0).getTextContent());
            result.add(new English_Section(name, size));

        }
        return result;
    }



    public IEnIn_Presenter getIEnIn_presenter() {
        return IEnIn_presenter;
    }

    public void setIEnIn_presenter(IEnIn_Presenter IEnIn_presenter) {
        this.IEnIn_presenter = IEnIn_presenter;
    }
}
