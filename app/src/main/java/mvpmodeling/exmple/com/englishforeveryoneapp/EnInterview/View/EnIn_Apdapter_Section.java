package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Section;
import mvpmodeling.exmple.com.englishforeveryoneapp.R;

/**
 * Created by kevin on 4/3/2016.
 */
public class EnIn_Apdapter_Section extends RecyclerView.Adapter<EnIn_Apdapter_Section.ViewHolder> {
    ArrayList<English_Section> data ;

    public EnIn_Apdapter_Section(ArrayList<English_Section> list) {
        data = list;
    }

    @Override
    public EnIn_Apdapter_Section.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View viewHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.enin_card_item_section, parent, false);

        ViewHolder vh = new ViewHolder(viewHolder);

        return vh;
    }

    @Override
    public void onBindViewHolder(EnIn_Apdapter_Section.ViewHolder holder, int position) {
        holder.setText(data.get(position).getSec_Name(),data.get(position).getSec_Size());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, size;
        ProgressBar progressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.info_text);
            size = (TextView) itemView.findViewById(R.id.infor_size);
            progressBar = (ProgressBar)itemView.findViewById(R.id.progressbar);
        }
        public void setText(String sname, double ssize)
        {
            name.setText(sname);
            size.setText("" + ssize);
        }

    }
}
