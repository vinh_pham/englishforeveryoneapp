package mvpmodeling.exmple.com.englishforeveryoneapp.Main.View;

import android.content.Context;

import java.util.ArrayList;

import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Systems;

/**
 * Created by kevin on 4/2/2016.
 */
public interface IMainActivity {
    void receiveListSystem(ArrayList<English_Systems> result);

    Context getContextA();
}
