package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View;

import org.jsoup.select.Elements;

import java.util.ArrayList;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model.SentenceInterview;

/**
 * Created by kevin on 4/4/2016.
 */
public interface IEnInItemInterview {

    void sendList(ArrayList<SentenceInterview> resultShortAnswer, ArrayList<SentenceInterview> resultlongAnswer);

    void receiveTemp(ArrayList<String> listSlipt, Elements a);
}
