package mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model;

import android.content.Context;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by kevin on 4/2/2016.
 */
@SuppressWarnings("serial")
public class English_Vocabulary extends English_Systems implements Serializable {


    public English_Vocabulary(String en_Name, double size) {
        super(en_Name, size);
    }

    @Override
    public String getNameEnlishSytem() {
        return this.En_Name;
    }

    @Override
    public double getSizeEnlishSytem() {
        Log.d("DEBUG", "English_Vocabulary getSizeEnlishSytem ");
        return getSizeSection();
    }

    private double getSizeSection() {
        return this.Size;
    }


    @Override
    public void setListSection(ArrayList<English_Section> list) {
        this.listSection = new ArrayList<>();
        Log.d("DEBUG", "English_Vocabulary setListSection ");
    }

    @Override
    public English_Section getSection(int position) {
        return this.listSection.get(position);
    }
}
