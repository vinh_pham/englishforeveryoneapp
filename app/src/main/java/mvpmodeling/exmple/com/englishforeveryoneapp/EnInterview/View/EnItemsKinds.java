package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Presenter.EnIn_Presenter;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.View.RecyclerItemClickListener;
import mvpmodeling.exmple.com.englishforeveryoneapp.R;

public class EnItemsKinds extends AppCompatActivity implements IEnItemsKindsView {


    private RecyclerView my_recycler_view;
    private AdapterItemKinds adapterItemKinds;
    private RecyclerView.LayoutManager layoutManager;
    private EnIn_Presenter presenter;

    final static String NAMEURL = "URLFILEHTML";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_en_items_kinds);

        my_recycler_view = (RecyclerView)this.findViewById(R.id.my_recycler_view);

        my_recycler_view.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        my_recycler_view.setLayoutManager(layoutManager);

        presenter = new EnIn_Presenter(this);
        presenter.GetItemsListKinds();

    }

    @Override
    public void receiveListKinds(ArrayList<String> data) {

        adapterItemKinds = new AdapterItemKinds(data);
        my_recycler_view.setAdapter(adapterItemKinds);
        my_recycler_view.addOnItemTouchListener( new RecyclerItemClickListener(this.getApplication(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // TODO Handle item click

                Intent intent = new Intent(EnItemsKinds.this, EnInItemInterview.class);
                switch (position) {
                    case 0:

                        intent.putExtra(NAMEURL, "basicI/zinterview1.html");
                        //intent.putExtra("ENG_PHRASE", EnSysList.get(position))
                        break;
                    case 1:
                        intent.putExtra(NAMEURL, "basicI/zinterview2.html");
                        break;
                    case 2:
                        intent.putExtra(NAMEURL, "basicI/zinterview3.html");
                        break;
                    case 3:
                        intent.putExtra(NAMEURL, "basicI/zinterview4.html");
                        break;
                    case 4:
                        intent.putExtra(NAMEURL, "basicI/zinterview5.html");
                        break;
                    case 5:

                        intent.putExtra(NAMEURL, "basicI/zinterview6.html");
                        //intent.putExtra("ENG_PHRASE", EnSysList.get(position))
                        break;
                    case 6:
                        intent.putExtra(NAMEURL, "basicI/zinterview7.html");
                        break;
                    case 7:
                        intent.putExtra(NAMEURL, "basicI/zinterview8.html");
                        break;
                    case 8:
                        intent.putExtra(NAMEURL, "basicI/zinterview9.html");
                        break;
                    case 9:
                        intent.putExtra(NAMEURL, "basicI/zinterview10.html");
                        break;
                    case 10:
                        intent.putExtra(NAMEURL, "basicI/zinterview11.html");
                        break;
                    case 11:

                        intent.putExtra(NAMEURL, "basicI/zinterview12.html");
                        //intent.putExtra("ENG_PHRASE", EnSysList.get(position))
                        break;
                    case 12:
                        intent.putExtra(NAMEURL, "basicI/zinterview13.html");
                        break;
                    case 13:
                        intent.putExtra(NAMEURL, "basicI/zinterview14.html");
                        break;
                    case 14:
                        intent.putExtra(NAMEURL, "basicI/zinterview15.html");
                        break;
                }
                startActivity(intent);
            }
        }));
    }
}
