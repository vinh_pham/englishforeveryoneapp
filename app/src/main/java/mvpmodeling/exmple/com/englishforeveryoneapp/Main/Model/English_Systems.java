package mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by kevin on 4/2/2016.
 */
@SuppressWarnings("serial")
public abstract class English_Systems implements Serializable {


    protected String En_Name;
    protected double Size;
    protected ArrayList<English_Section> listSection;

    public English_Systems(String en_Name, double size) {
        En_Name = en_Name;
        Size = size;
    }



    public abstract String getNameEnlishSytem();
    public abstract double getSizeEnlishSytem();
    public abstract void setListSection(ArrayList<English_Section> list);
    public abstract English_Section getSection(int position);
}
