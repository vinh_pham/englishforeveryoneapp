package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model.SentenceInterview;
import mvpmodeling.exmple.com.englishforeveryoneapp.R;

/**
 * Created by kevin on 4/6/2016.
 */
public class AdapterListViewTesting extends BaseAdapter {
    private final Context context;
    ArrayList<SentenceInterview> test;
    public AdapterListViewTesting(Context context, ArrayList<SentenceInterview> test) {
        this.test = test;
        this.context = context;
    }

    @Override
    public int getCount() {
        return test.size();
    }

    @Override
    public Object getItem(int position) {
        return test.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder{
        TextView text;
        Button But_play;
        String uriS;


        public void SetData(String text, String uri)
        {
            this.uriS = uri;
            this.text.setText(text);
            this.But_play.setOnClickListener(new View.OnClickListener() {
                String uri = uriS;

                @Override
                public void onClick(View v) {
                    MediaPlayer mediaPlayer = new MediaPlayer();
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    try {
                        mediaPlayer.setDataSource(this.uri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        mediaPlayer.prepare(); // might take long! (for buffering, etc)
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mediaPlayer.start();
                }
            });
        }
        public ViewHolder(View v) {

            this.text = (TextView)v.findViewById(R.id.txt_sentence);
            this.But_play = (Button)v.findViewById(R.id.but_play);

        }
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater minflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = minflater.inflate(R.layout.fragment_short_answer_card_item, null);
            holder = new ViewHolder(convertView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.SetData(test.get(position).getSentence(), test.get(position).getUri());
        return convertView;
    }
}
