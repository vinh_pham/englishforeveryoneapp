package mvpmodeling.exmple.com.englishforeveryoneapp.Main.View;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Systems;
import mvpmodeling.exmple.com.englishforeveryoneapp.R;

/**
 * Created by kevin on 4/2/2016.
 */
public class MainApdapterCard extends RecyclerView.Adapter<MainApdapterCard.ViewHolder> {
    ArrayList<English_Systems> data;

    public MainApdapterCard(ArrayList<English_Systems> result) {
        this.data = result;
    }

    @Override
    public MainApdapterCard.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_en_card_itemlist, parent, false);
        // set the view's size, margins, paddings and layout parameter
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MainApdapterCard.ViewHolder holder, int position) {
        holder.setText(data.get(position).getNameEnlishSytem(),data.get(position).getSizeEnlishSytem());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView mName;
        TextView mSize;
        ProgressBar mprogress;

        public ViewHolder(View itemView) {
            super(itemView);

            mName = (TextView)itemView.findViewById(R.id.info_text);
            mSize = (TextView)itemView.findViewById(R.id.infor_size);
            mprogress = (ProgressBar)itemView.findViewById(R.id.progressbar);
        }
        public void setText(String Name, Double size)
        {
            mprogress.setVisibility(View.INVISIBLE);
            mName.setText(Name);
            mSize.setText("" + size);
        }
    }
}
