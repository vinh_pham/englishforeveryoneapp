package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View;

import java.util.ArrayList;

/**
 * Created by kevin on 4/8/2016.
 */
public interface IEnItemsKindsView {
    void receiveListKinds(ArrayList<String> data);
}
