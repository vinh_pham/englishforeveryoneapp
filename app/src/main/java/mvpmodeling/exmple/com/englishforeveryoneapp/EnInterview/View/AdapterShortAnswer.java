package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model.SentenceInterview;
import mvpmodeling.exmple.com.englishforeveryoneapp.R;

/**
 * Created by kevin on 4/4/2016.
 */
public class AdapterShortAnswer extends RecyclerView.Adapter<AdapterShortAnswer.ViewHolder> {
    ArrayList<SentenceInterview> aListShortAnswer;
    public AdapterShortAnswer(ArrayList<SentenceInterview> aListShortAnswer) {
        this.aListShortAnswer = aListShortAnswer;
    }

    @Override
    public AdapterShortAnswer.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_short_answer_card_item,parent,false);

        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(aListShortAnswer.get(position).getSentence());
    }

    
    @Override
    public int getItemCount() {
        return aListShortAnswer.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView text_sentence;
        Button butPlay, butPause, butStop;
        public ViewHolder(View itemView) {
            super(itemView);
            
            text_sentence = (TextView)itemView.findViewById(R.id.txt_sentence);
            butPlay = (Button)itemView.findViewById(R.id.but_play);
            butPause = (Button)itemView.findViewById(R.id.but_pause);
            butStop = (Button)itemView.findViewById(R.id.but_stop);
        }
        public void setData(String tex){
            text_sentence.setText(tex);
        }
    }
}
