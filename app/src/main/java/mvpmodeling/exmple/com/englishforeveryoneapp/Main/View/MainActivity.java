package mvpmodeling.exmple.com.englishforeveryoneapp.Main.View;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View.EnInterview;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Model.English_Systems;
import mvpmodeling.exmple.com.englishforeveryoneapp.Main.Presenter.MainPresenter;
import mvpmodeling.exmple.com.englishforeveryoneapp.R;

public class MainActivity extends AppCompatActivity implements IMainActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter myAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private MainPresenter mainpresenter;
    static ArrayList<English_Systems> EnSysList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);


        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mainpresenter = new MainPresenter(this);
        mainpresenter.GetListSystem();
    }

    @Override
    public void receiveListSystem(final ArrayList<English_Systems> result) {

        EnSysList = result;
        myAdapter = new MainApdapterCard(result);
        mRecyclerView.setAdapter(myAdapter);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this.getApplication(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // TODO Handle item click

                        switch (position)
                        {
                            case 0:
                                Intent intent = new Intent(MainActivity.this, EnInterview.class);

                                intent.putExtra("ENG_PHRASE",EnSysList.get(position) );
                                startActivity(intent);

                                break;
                            case 1:
                                break;
                        }

                    }
                })
        );


    }
    @Override
    public Context getContextA() {
        return this.getApplicationContext();
    }
}
