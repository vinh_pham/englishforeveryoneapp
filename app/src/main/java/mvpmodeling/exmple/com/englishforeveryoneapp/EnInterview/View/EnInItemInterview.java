package mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.View;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import org.jsoup.select.Elements;

import java.util.ArrayList;

import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Model.SentenceInterview;
import mvpmodeling.exmple.com.englishforeveryoneapp.EnInterview.Presenter.EnIn_Presenter;
import mvpmodeling.exmple.com.englishforeveryoneapp.R;

public class EnInItemInterview extends AppCompatActivity implements ShortAnswer.OnFragmentInteractionListener, LongAnswer.OnFragmentInteractionListener, IEnInItemInterview {

    ViewPager viewPager;
    PagerTabStrip tab_strip;
    EnIn_Presenter pressenter;
    String NameURL = "basicI/zinterview1.html";
    final static String NAMEURL = "URLFILEHTML";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_en_in_item_interview);

        viewPager = (ViewPager)findViewById(R.id.pager);
        tab_strip = (PagerTabStrip)findViewById(R.id.tab_strip);
        if(this.getIntent() != null)
        {
            Intent it = getIntent();
            NameURL = it.getStringExtra(NAMEURL);
        }

        pressenter = new EnIn_Presenter(this);
        pressenter.GetListSentenceAnswer(NameURL);


    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void sendList(ArrayList<SentenceInterview> resultShortAnswer, ArrayList<SentenceInterview> resultlongAnswer) {
        if(resultShortAnswer == null || resultlongAnswer == null)
            return;
        viewPager.setAdapter(new AdapterTabStrip(getSupportFragmentManager(), resultShortAnswer, resultlongAnswer));


    }

    @Override
    public void receiveTemp(ArrayList<String> listSlipt, Elements a) {

      //  Toast.makeText(getApplicationContext(),"List: "+ listSlipt.size() +"s1111 " + listSlipt.get(0)  + "-"  + "\\ a: " + a.size(),Toast.LENGTH_SHORT).show();
    }
}
